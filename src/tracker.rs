use std::{
    io::{Read, Write},
    net::{TcpListener, TcpStream},
    thread,
};

pub fn start() -> String {
    let listener = TcpListener::bind(format!("localhost:0")).unwrap();
    let tracker_addr = listener.local_addr().unwrap().to_string();
    println!("tracker => started at {}", tracker_addr);

    thread::spawn(move || {
        let mut streamers: Vec<String> = Vec::new();
        for conn in listener.incoming() {
            let socket = conn.unwrap();
            handle_connection(socket, &mut streamers);
        }
    });

    return tracker_addr;
}

fn handle_connection(mut socket: TcpStream, streamers: &mut Vec<String>) {
    let mut buf: [u8; 32] = [0; 32];
    socket.read(&mut buf).unwrap();

    if buf[0] == u8::MAX {
        // streamer publish
        let mut last = 1;
        while last < 32 && buf[last] != 0 {
            last += 1;
        }
        let content: Vec<u8> = buf[1..last].to_vec();
        let streamer_addr = String::from_utf8(content).unwrap();
        println!("tracker => adding streamer {}", streamer_addr);
        streamers.push(streamer_addr);
    } else {
        // client request
        let streamer_addr = streamers.get(buf[0] as usize).unwrap();
        socket.write(streamer_addr.as_bytes()).unwrap();

        let peer = socket.peer_addr().unwrap();
        println!(
            "tracker => watcher {} requested to view streamer {}",
            peer, streamer_addr
        );
    }
}
