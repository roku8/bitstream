use std::thread;
use std::time::Duration;

mod tracker;
mod streamer;
mod watcher;

fn main() {
    let tracker = tracker::start();
    thread::sleep(Duration::from_secs(1));

    streamer::start(tracker.to_owned());
    thread::sleep(Duration::from_secs(1));

    for _ in 0..5 {
        let tracker = tracker.to_owned();
        thread::spawn(move || {
            watcher::start(tracker);
        });
        thread::sleep(Duration::from_secs(1));
    }

    thread::sleep(Duration::from_secs(60));
}
