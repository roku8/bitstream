use std::{
    io::{Read, Write},
    net::{TcpStream, UdpSocket},
    thread,
};

pub fn start(tracker_addr: String) {
    let streamer_addr = get_streamer(tracker_addr);
    let data_socket = UdpSocket::bind("localhost:0").unwrap();
    let data_addr = data_socket.local_addr().unwrap().to_string();

    println!("watcher => receiving data on {}", data_addr);
    connect_streamer(streamer_addr, data_addr);

    thread::spawn(move || loop {
        let mut data = [0; 5];
        let (_, streamer) = data_socket.recv_from(&mut data).unwrap();
        println!(
            "watcher => received data {:?} from streamer {}",
            data, streamer
        );
    });
}

fn get_streamer(tracker_addr: String) -> String {
    println!(
        "watcher => requesting tracker {} for streamers",
        tracker_addr
    );

    let mut conn = TcpStream::connect(tracker_addr).unwrap();
    let request = vec![0];
    conn.write(&request).unwrap();

    let mut resp = [0; 32];
    conn.read(&mut resp).unwrap();

    let mut last = 0;
    while last < 32 && resp[last] != 0 {
        last += 1;
    }
    let content = resp[0..last].to_vec();

    return String::from_utf8(content).unwrap();
}

fn connect_streamer(streamer_addr: String, watcher_addr: String) {
    println!("watcher => connecting to streamer {}", streamer_addr);

    let mut conn = TcpStream::connect(streamer_addr).unwrap();
    let mut request = vec![u8::MAX];
    for &b in watcher_addr.as_bytes() {
        request.push(b);
    }
    conn.write(&request).unwrap();
}
