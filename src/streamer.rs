use rand::Rng;
use std::{
    io::{Read, Write},
    net::{TcpListener, TcpStream, UdpSocket},
    sync::{Arc, RwLock},
    thread,
    time::Duration,
};

pub fn start(tracker_addr: String) {
    let registration_socket = TcpListener::bind("localhost:0").unwrap();
    let registration_addr = registration_socket.local_addr().unwrap().to_string();
    println!("streamer => listening on {}", registration_addr);
    publish(tracker_addr, registration_addr);

    let watchers: Arc<RwLock<Vec<String>>> = Arc::new(RwLock::new(Vec::new()));
    let publish_socket = UdpSocket::bind("localhost:0").unwrap();
    println!(
        "streamer => publishing data on {}",
        publish_socket.local_addr().unwrap().to_string()
    );

    let watchers_wr = Arc::clone(&watchers);
    thread::spawn(move || {
        for conn in registration_socket.incoming() {
            let socket = conn.unwrap();
            add_watcher(socket, &mut watchers_wr.write().unwrap());
        }
    });

    let watchers_rd = Arc::clone(&watchers);
    thread::spawn(move || {
        let mut data: [u8; 5] = [0; 5];

        loop {
            rand::thread_rng().fill(&mut data);

            for watcher in &*watchers_rd.read().unwrap() {
                println!("streamer => sending data to {}", watcher);
                publish_socket.send_to(&data, watcher).unwrap();
            }

            thread::sleep(Duration::from_millis(1000));
        }
    });
}

fn publish(tracker_addr: String, server_addr: String) {
    let mut conn = TcpStream::connect(tracker_addr).unwrap();
    let mut request = vec![u8::MAX];
    for &b in server_addr.as_bytes() {
        request.push(b);
    }
    conn.write(&request).unwrap();
}

fn add_watcher(mut socket: TcpStream, watchers: &mut Vec<String>) {
    let mut buf: [u8; 32] = [0; 32];
    socket.read(&mut buf).unwrap();

    if buf[0] == u8::MAX {
        // streamer publish
        let mut last = 1;
        while last < 32 && buf[last] != 0 {
            last += 1;
        }
        let content: Vec<u8> = buf[1..last].to_vec();
        let watcher_addr = String::from_utf8(content).unwrap();
        {
            println!("streamer => adding watcher {}", watcher_addr);
            watchers.push(watcher_addr);
        }
    }
}
